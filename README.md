This project about implementing end-to-end testing automation with cypress and drone

## Available Scripts
In the project directory, you can run:
- Run cypress
    - `yarn run cypress open`
    - `yarn run cypress run --record --key <record key>`

DOCKER DRONE CL
===============
## Requirements
- [docker](https://docs.docker.com/install/)
- [docker-compose](https://docs.docker.com/compose/install/)
- [ngrok](https://ngrok.com/) if run inside firewall

## Usage:
- Configure the Drone Server's Environment Variable File
    - We should generate a strong key to authenticate the agent and server components.
        ```
        LC_ALL=C </dev/urandom tr -dc A-Za-z0-9 | head -c 32 && echo
        ```
    - Create a new file at `path_to_file/server.env` and input Variables for server
        ```
        DRONE_OPEN=true
        DRONE_SECRET=secret_generated_above
        DRONE_HOST=http://your_domain.com
        ```
        - `DRONE_HOST` is an IP or domain that can access it from the Internet.

    - Integrate with Github
        - Get client/secret key: https://developer.github.com/apps/building-oauth-apps/creating-an-oauth-app/
        ```
        DRONE_GITHUB=true
        DRONE_GITHUB_CLIENT=github_client_key
        DRONE_GITHUB_SECRET=github_secret
        ```
    - Integrate with Gitlab
        - Get client/secret key: https://docs.gitlab.com/ee/integration/oauth_provider.html
        ```
        DRONE_GITLAB=true
        DRONE_GITLAB_CLIENT=gitlab_client_key
        DRONE_GITLAB_SECRET=gitlab_secret key
        DRONE_GITLAB_URL=https://gitlab.com or your gitlab domain
        ```
    * Notes:
        - CAllBACK URL : use http://domain.com/authorize instead for http://domain.com/login

- Configure the Drone Agent's Environment Variable File
    - Create a new file at `path_to_file/agent.env` and input Variables for agent
        ```
        DRONE_SECRET=same_with_server
        DRONE_SERVER=drone-server:9000
        ```
- Variables file path of `server.env` and `agent.env` should be same with `docker-compose.yml` file. If not, must change path of ``env_file` in `docker-compose.yml` too

- Run drone server and agent
    ```
    docker-compose up
    ```
- Open web browser with your `domain/ip` to view the Drone web interface. Done
- ngrok http 80

